Changelog
=========


0.6.59 (2024-06-24)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.58' into 'main' [Marvin van Aalst]

  0.6.58

  See merge request marvin.vanaalst/hue-sunrise!72
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.58 (2024-06-10)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.57' into 'main' [Marvin van Aalst]

  0.6.57

  See merge request marvin.vanaalst/hue-sunrise!71
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.57 (2024-06-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.56' into 'main' [Marvin van Aalst]

  0.6.56

  See merge request marvin.vanaalst/hue-sunrise!70
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.56 (2024-05-27)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.55' into 'main' [Marvin van Aalst]

  0.6.55

  See merge request marvin.vanaalst/hue-sunrise!69
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.55 (2024-05-20)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.54' into 'main' [Marvin van Aalst]

  0.6.54

  See merge request marvin.vanaalst/hue-sunrise!68
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.54 (2024-05-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.53' into 'main' [Marvin van Aalst]

  0.6.53

  See merge request marvin.vanaalst/hue-sunrise!67
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.53 (2024-05-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.52' into 'main' [Marvin van Aalst]

  0.6.52

  See merge request marvin.vanaalst/hue-sunrise!66
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.52 (2024-04-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.51' into 'main' [Marvin van Aalst]

  0.6.51

  See merge request marvin.vanaalst/hue-sunrise!65
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.51 (2024-04-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.50' into 'main' [Marvin van Aalst]

  0.6.50

  See merge request marvin.vanaalst/hue-sunrise!64
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.50 (2024-03-18)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.49' into 'main' [Marvin van Aalst]

  0.6.49

  See merge request marvin.vanaalst/hue-sunrise!63
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.49 (2024-03-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.48' into 'main' [Marvin van Aalst]

  0.6.48

  See merge request marvin.vanaalst/hue-sunrise!62
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.48 (2024-02-26)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.47' into 'main' [Marvin van Aalst]

  0.6.47

  See merge request marvin.vanaalst/hue-sunrise!61
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.47 (2024-02-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.46' into 'main' [Marvin van Aalst]

  0.6.46

  See merge request marvin.vanaalst/hue-sunrise!60
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.46 (2024-02-05)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.45' into 'main' [Marvin van Aalst]

  0.6.45

  See merge request marvin.vanaalst/hue-sunrise!59
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.45 (2024-01-29)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.44' into 'main' [Marvin van Aalst]

  0.6.44

  See merge request marvin.vanaalst/hue-sunrise!58
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.44 (2024-01-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.43' into 'main' [Marvin van Aalst]

  0.6.43

  See merge request marvin.vanaalst/hue-sunrise!57
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.43 (2024-01-01)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.42' into 'main' [Marvin van Aalst]

  0.6.42

  See merge request marvin.vanaalst/hue-sunrise!56
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.42 (2023-12-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.41' into 'main' [Marvin van Aalst]

  0.6.41

  See merge request marvin.vanaalst/hue-sunrise!55
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.41 (2023-12-20)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Dev: chg: unified gitlab ci. [Marvin van Aalst]
- Merge branch 'v0.6.40' into 'main' [Marvin van Aalst]

  0.6.40

  See merge request marvin.vanaalst/hue-sunrise!54
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.40 (2023-12-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Bot: chg: updated pre-commit. [Marvin van Aalst]
- Merge branch 'v0.6.39' into 'main' [Marvin van Aalst]

  0.6.39

  See merge request marvin.vanaalst/hue-sunrise!53
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.39 (2023-11-13)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: pre-commit. [Marvin van Aalst]
- Dev: chg: unified black settings. [Marvin van Aalst]
- Dev: chg: black. [Marvin van Aalst]
- Dev: chg: unified black settings. [Marvin van Aalst]
- Dev: new: included security checks with bandit. [Marvin van Aalst]
- Merge branch 'v0.6.38' into 'main' [Marvin van Aalst]

  0.6.38

  See merge request marvin.vanaalst/hue-sunrise!52
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.38 (2023-11-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.37' into 'main' [Marvin van Aalst]

  0.6.37

  See merge request marvin.vanaalst/hue-sunrise!51
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.37 (2023-10-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.36' into 'main' [Marvin van Aalst]

  0.6.36

  See merge request marvin.vanaalst/hue-sunrise!50
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.36 (2023-10-23)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.35' into 'main' [Marvin van Aalst]

  0.6.35

  See merge request marvin.vanaalst/hue-sunrise!49
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.35 (2023-10-16)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.34' into 'main' [Marvin van Aalst]

  0.6.34

  See merge request marvin.vanaalst/hue-sunrise!48
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.34 (2023-10-09)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.33' into 'main' [Marvin van Aalst]

  0.6.33

  See merge request marvin.vanaalst/hue-sunrise!47
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.33 (2023-10-02)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.32' into 'main' [Marvin van Aalst]

  0.6.32

  See merge request marvin.vanaalst/hue-sunrise!46
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.32 (2023-09-25)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.31' into 'main' [Marvin van Aalst]

  0.6.31

  See merge request marvin.vanaalst/hue-sunrise!45
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.31 (2023-09-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.30' into 'main' [Marvin van Aalst]

  0.6.30

  See merge request marvin.vanaalst/hue-sunrise!44
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.30 (2023-09-04)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.29' into 'main' [Marvin van Aalst]

  0.6.29

  See merge request marvin.vanaalst/hue-sunrise!43
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.29 (2023-08-28)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.28' into 'main' [Marvin van Aalst]

  0.6.28

  See merge request marvin.vanaalst/hue-sunrise!42
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.28 (2023-08-21)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.27' into 'main' [Marvin van Aalst]

  0.6.27

  See merge request marvin.vanaalst/hue-sunrise!41
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.27 (2023-08-14)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.26' into 'main' [Marvin van Aalst]

  0.6.26

  See merge request marvin.vanaalst/hue-sunrise!40
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.26 (2023-07-31)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.25' into 'main' [Marvin van Aalst]

  0.6.25

  See merge request marvin.vanaalst/hue-sunrise!39
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.25 (2023-07-24)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.24' into 'main' [Marvin van Aalst]

  0.6.24

  See merge request marvin.vanaalst/hue-sunrise!38
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.24 (2023-07-17)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.23' into 'main' [Marvin van Aalst]

  0.6.23

  See merge request marvin.vanaalst/hue-sunrise!37
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.23 (2023-07-10)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.22' into 'main' [Marvin van Aalst]

  0.6.22

  See merge request marvin.vanaalst/hue-sunrise!36
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.22 (2023-07-03)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.21' into 'main' [Marvin van Aalst]

  0.6.21

  See merge request marvin.vanaalst/hue-sunrise!35
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.21 (2023-06-26)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.20' into 'main' [Marvin van Aalst]

  0.6.20

  See merge request marvin.vanaalst/hue-sunrise!34
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.20 (2023-06-19)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.19' into 'main' [Marvin van Aalst]

  0.6.19

  See merge request marvin.vanaalst/hue-sunrise!33
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.19 (2023-06-12)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.18' into 'main' [Marvin van Aalst]

  0.6.18

  See merge request marvin.vanaalst/hue-sunrise!32
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.18 (2023-06-05)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.17' into 'main' [Marvin van Aalst]

  0.6.17

  See merge request marvin.vanaalst/hue-sunrise!31
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.17 (2023-05-30)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.16' into 'main' [Marvin van Aalst]

  0.6.16

  See merge request marvin.vanaalst/hue-sunrise!30
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.16 (2023-05-15)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.15' into 'main' [Marvin van Aalst]

  0.6.15

  See merge request marvin.vanaalst/hue-sunrise!29
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.15 (2023-05-08)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.14' into 'main' [Marvin van Aalst]

  0.6.14

  See merge request marvin.vanaalst/hue-sunrise!28
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.14 (2023-05-01)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.13' into 'main' [Marvin van Aalst]

  0.6.13

  See merge request marvin.vanaalst/hue-sunrise!27
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.13 (2023-04-11)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.12' into 'main' [Marvin van Aalst]

  0.6.12

  See merge request marvin.vanaalst/hue-sunrise!26
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.12 (2023-03-31)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.11' into 'main' [Marvin van Aalst]

  0.6.11

  See merge request marvin.vanaalst/hue-sunrise!25
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.11 (2023-03-13)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.10' into 'main' [Marvin van Aalst]

  0.6.10

  See merge request marvin.vanaalst/hue-sunrise!24
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.10 (2023-03-06)
-------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.9' into 'main' [Marvin van Aalst]

  0.6.9

  See merge request marvin.vanaalst/hue-sunrise!23
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.9 (2023-02-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.8' into 'main' [Marvin van Aalst]

  0.6.8

  See merge request marvin.vanaalst/hue-sunrise!22
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.8 (2023-02-06)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.7' into 'main' [Marvin van Aalst]

  0.6.7

  See merge request marvin.vanaalst/hue-sunrise!21
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.7 (2023-01-30)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.6' into 'main' [Marvin van Aalst]

  0.6.6

  See merge request marvin.vanaalst/hue-sunrise!20
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.6 (2023-01-23)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.5' into 'main' [Marvin van Aalst]

  0.6.5

  See merge request marvin.vanaalst/hue-sunrise!19
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.5 (2023-01-16)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.4' into 'main' [Marvin van Aalst]

  0.6.4

  See merge request marvin.vanaalst/hue-sunrise!18
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.4 (2023-01-09)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.3' into 'main' [Marvin van Aalst]

  0.6.3

  See merge request marvin.vanaalst/hue-sunrise!17
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.3 (2023-01-02)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.2' into 'main' [Marvin van Aalst]

  0.6.2

  See merge request marvin.vanaalst/hue-sunrise!16
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.2 (2022-12-27)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.1' into 'main' [Marvin van Aalst]

  0.6.1

  See merge request marvin.vanaalst/hue-sunrise!15
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.1 (2022-12-19)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.6.0' into 'main' [Marvin van Aalst]

  0.6.0

  See merge request marvin.vanaalst/hue-sunrise!14
- Bot: chg: updated changelog. [Marvin van Aalst]


0.6.0 (2022-12-13)
------------------
- Usr: fix: lights are now correctly set when calling sunrise via CLI.
  [Marvin van Aalst]
- Dev: fix: naming. [Marvin van Aalst]
- Bot: chg: updated changelog. [Marvin van Aalst]


0.5.0 (2022-12-12)
------------------
- Usr: new: env variables for log and config files. [Marvin van Aalst]
- Merge branch 'v0.4.1' into 'main' [Marvin van Aalst]

  0.4.1

  See merge request marvin.vanaalst/hue-sunrise!13
- Bot: chg: updated changelog. [Marvin van Aalst]


0.4.1 (2022-12-12)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'v0.4.0' into 'main' [Marvin van Aalst]

  V0.4.0

  See merge request marvin.vanaalst/hue-sunrise!12
- Bot: chg: updated changelog. [Marvin van Aalst]


0.4.0 (2022-12-09)
------------------
- Usr: fix: optional instead of pipe syntax for typer. [Marvin van
  Aalst]
- Merge branch 'v0.3.0' into 'main' [Marvin van Aalst]

  V0.3.0

  See merge request marvin.vanaalst/hue-sunrise!11
- Bot: chg: updated changelog. [Marvin van Aalst]


0.3.0 (2022-12-09)
------------------
- Usr: new: directly change some configs in run command. [Marvin van
  Aalst]
- Merge branch 'v0.2.0' into 'main' [Marvin van Aalst]

  0.2.0

  See merge request marvin.vanaalst/hue-sunrise!10
- Bot: chg: updated changelog. [Marvin van Aalst]
- Usr: new: config to set user directly. [Marvin van Aalst]
- Merge branch 'v0.1.9' into 'main' [Marvin van Aalst]

  0.1.9

  See merge request marvin.vanaalst/hue-sunrise!9
- Bot: chg: updated changelog. [Marvin van Aalst]


0.2.0 (2022-12-02)
------------------
- Usr: new: config to set user directly. [Marvin van Aalst]


0.1.9 (2022-11-29)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.8

  See merge request marvin.vanaalst/hue-sunrise!8
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.8 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated python version bound. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.7

  See merge request marvin.vanaalst/hue-sunrise!7
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.7 (2022-11-21)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.6

  See merge request marvin.vanaalst/hue-sunrise!6
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.6 (2022-11-14)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.5

  See merge request marvin.vanaalst/hue-sunrise!5
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.5 (2022-11-07)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.4

  See merge request marvin.vanaalst/hue-sunrise!4
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.4 (2022-10-31)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.3

  See merge request marvin.vanaalst/hue-sunrise!3
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.3 (2022-10-26)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: updated ci container. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.2

  See merge request marvin.vanaalst/hue-sunrise!2
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.2 (2022-10-24)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Merge branch 'dependencies' into 'main' [Marvin van Aalst]

  0.1.1

  See merge request marvin.vanaalst/hue-sunrise!1
- Bot: chg: updated changelog. [Marvin van Aalst]


0.1.1 (2022-09-27)
------------------
- Bot: chg: updated dependencies. [Marvin van Aalst]
- Dev: chg: added changelog to dev dependencies. [Marvin van Aalst]
- Dev: new: initial commit. [Marvin van Aalst]


